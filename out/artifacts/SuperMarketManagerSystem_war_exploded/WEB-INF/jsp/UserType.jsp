<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    %>
    <base href="<%=basePath%>">
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <script src="http://www.jq22.com/jquery/jquery-1.10.2.js"/>
    <script type="text/javascript" src="<%=basePath%>js/user.js"></script>
    <script type="text/javascript" src="<%=basePath%>js/del.js"></script>
    <link rel="stylesheet" href="<%=basePath%>css/public.css"/>
    <link rel="stylesheet" href="<%=basePath%>css/style.css"/>
    <style type="text/css">
        .providerTable {
            width: 100%;
        }

        .tablebody {
            width: 100%;
        }
        a {
            display:inline-block;
            margin:0px 0px 5px 5px;
            padding:6px 8px;
            font-size:14px;
            outline:none;
            text-align:center;
            /*width:50px;*/
            /*line-height:30px;*/
            cursor: pointer;
        }

        a {
            color:white;
            background-color:rgba(0,64,156,.8);
        }

        a:hover {
            color:white;
            background-color:rgba(255,0,0,.8);
        }
    </style>
</head>


<body>
<div class="search">
<%--    类型级别：<select id="level-select"></select>--%>
<%--    商品类型：<select id="example-select"></select>--%>
    <a href="addUserType">添加用户类型</a>
</div>

<div class="providerTable" style="overflow-x: auto; overflow-y: auto;">
    <div class="tablebodyBox">
        <table class="tablebody" id="table1" cellpadding="0" cellspacing="0">
            <tr class="firstTr">
                <th width="10%">序号</th>
                <th width="10%">用户类型ID</th>
                <th width="20%">用户类型</th>
                <th width="10%">备注</th>
                <th width="10%">操作</th>
            </tr>
        </table>
    </div>
</div>
</div>
</body>
<script type="text/javascript">

    function deleteAllRowsOfTable(t_id) {
        var table = document.getElementById(t_id);
        var len = table.rows.length;
        for (var i = len - 1; i > 0; i--) {
            table.deleteRow(i);
        }
    }
    // var menu_op = $("#example-select");
    // var level_op = $("#level-select");
    // var example_array = [];
    // var level_array = [];
    $(document).ready(function () {

        //goodsType
        $.getJSON("selectAllRole", function (res) {
            console.log(res);
            var table = $("#table1");
            $(res.o_list).each(function (i, item) {
                item.note=item.note==null?"":item.note;
                table.append("<tr><td>" + (i +1) +
                    "</td><td>" + item.id +
                    "</td><td>" + item.rolename +
                    "</td><td>" + item.note +
                    "</td><td><a href='updateUserType?id="+item.id+"'>修改</a><a href='deleteRoleById?id="+item.id+"'  onclick='return del()'>删除</a>" +
                    "</td></tr>")
            });
        });

        //点击查询按钮
        $("#id_queryProvider").click(function () {
            var providerName = $("#id_providerText").val();
            deleteAllRowsOfTable("table1");
            $.getJSON("selectProviderByName", {"name": providerName}, function (res) {
                console.log(res);
                item.note=item.note==null?"":item.note;
                var table = $("#table1");
                $(res.o_list).each(function (i, item) {
                    table.append("<tr><td>" + (i +1) +
                        "</td><td>" + item.id +
                        "</td><td>" + item.rolename +
                        "</td><td>" + item.note +
                        "</td><td><a href='updateUserType?id="+item.id+"'>修改</a><a href='deleteRoleById?id="+item.id+"'  onclick='return del()'>删除</a>" +
                        "</td></tr>")
                });
            });
        });
    });


</script>
</html>
