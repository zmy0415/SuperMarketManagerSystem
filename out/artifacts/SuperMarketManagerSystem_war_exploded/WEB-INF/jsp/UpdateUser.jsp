<%@ page import="com.zmy.pojo.Page" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
</head>
<body>

    <div>
        <form action="updateUser" name="twForm" id="myform" >
            <table style="margin: 40px auto;">
                <tr style="display: none;">
                    <td>id:</td>
                    <td><input type="text" name="id" value="${USER.id}"></td>
                </tr>
                <tr>
                    <td>帐号（英文）:</td>
                    <td><input type="text" name="code" value="${USER.code}"></td>
                </tr>
                <tr>
                    <td>名字:</td>
                    <td><input type="text" name="name"  value="${USER.name}"></td>
                </tr>
                <tr>
                    <td>密码:</td>
                    <td><input type="text" name="password"  value="${USER.password}"</td>
                </tr>
                <tr>
                    <td>地址:</td>
                    <td><input type="text" name="address"  value="${USER.address}"></td>
                </tr>
                <tr>
                    <td>电话:</td>
                    <td><input type="text" name="telephone"  value="${USER.telephone}"></td>
                </tr>
                <tr>
                    <td>性别:</td>
                    <td>
                        <input type="text" id="input_sex" name="gender"  style="display: none;" >
                        <select id="select_sex" >
                            <option value="1">男</option>
                            <option value="0">女</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>年龄:</td>
                    <td><input type="text" name="age" value="${USER.age}"></td>
                </tr>
                <tr>
                    <td>角色:</td>
                    <td>
                        <input type="text" id="input_role" name="role_id" style="display: none;"/>
                        <select id="id_select_role"></select>
                    </td>
                </tr>
                <tr>
                    <td  align="center"><input type="submit" style="margin-top: 5px;" value="修改"></td>
                    <td><button onclick="fanhui()">返回</button></td>
                </tr>

            </table>

        </form>
    </div>
</body>
<script>



    function fanhui(){
        window.history.back();
    }

    var arr_sex=["女","男"];
    $(document).ready(
        function (){
            $("#input_sex").val('${USER.gender}');
            $("#select_sex ").val('${USER.gender}');
        }
    );
    $("#select_sex").change(function () {
        var v = $("#select_sex option:selected").val();
        $("#input_sex").val(v);
    });
    //用户类型
    $("#id_select_role").change(function () {
        var v = $("#id_select_role option:selected").val();
        $("#input_role").val(v);
    });


    $.getJSON("selectAllRole",function (res){
        //设置到select
        $("#input_role").val(res.o_list[0].id);
        $(res.o_list).each(function (i,item){
            $("#id_select_role").append("<option value='"+item.id+"'>"+item.rolename+"</option>");
        });


    });
</script>
</html>
