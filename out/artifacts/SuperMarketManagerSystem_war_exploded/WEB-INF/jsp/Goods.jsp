<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    %>
    <base href="<%=basePath%>">
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <script src="http://www.jq22.com/jquery/jquery-1.10.2.js"/>
    <script type="text/javascript" src="<%=basePath%>js/user.js"></script>
    <link rel="stylesheet" href="<%=basePath%>css/public.css"/>
    <link rel="stylesheet" href="<%=basePath%>css/style.css"/>
    <style type="text/css">
        .providerTable {
            width: 100%;
        }

        .tablebody {
            width: 100%;
        }
        th,td{
            text-align: center;
        }
    </style>
</head>


<body>
<div class="search">
    <span>商品名称：</span>
    <input type="text" id="id_providerText" placeholder="请输入商品的名称"/>
    <input type="button" value="查询" id="id_queryProvider"/>
    <a href="toAddGoods">添加商品</a>

</div>
<!--供应商操作表格-->


<%--商品名称、类型、单位、数量、采购价、出售价、库存下限、生产厂商、备注--%>
<div class="providerTable" style="overflow-x: auto; overflow-y: auto;">
    <div class="tablebodyBox">
        <table class="tablebody" id="table1" cellpadding="0" cellspacing="0">
            <tr class="firstTr">
                <th width="10%">序号</th>
                <th width="10%">商品名称</th>
                <th width="10%">类型</th>
                <th width="10%">单位</th>
                <th width="10%">数量</th>
                <th width="10%">采购价</th>
                <th width="10%">出售价</th>
                <th width="10%">库存下限</th>
                <th width="10%">生产厂商</th>
                <th width="10%">备注</th>
            </tr>
        </table>
    </div>
</div>
</div>
</body>
<script type="text/javascript">




    function deleteAllRowsOfTable(t_id) {
        var table = document.getElementById(t_id);
        var len = table.rows.length;
        for (var i = len - 1; i > 0; i--) {
            table.deleteRow(i);
        }
    }

    $(document).ready(function () {
        $.getJSON("getAllGoods", function (res) {
            //console.log(res);
            var table = $("#table1");
            $(res.o_list).each(function (i, item) {
                item.goods_name = item.goods_name==null?"":item.goods_name;
                item.goodsType.type_name = item.goodsType.type_name==null?"":item.goodsType.type_name;
                item.goods_unit = item.goods_unit==null?"":item.goods_unit;
                item.goods_totalnum = item.goods_totalnum==null?"":item.goods_totalnum;
                item.goods_price=item.goods_price==null?"":item.goods_price;
                item.goods_purchase_price=item.goods_purchase_price==null?"":item.goods_purchase_price;
                item.lower_bounds=item.lower_bounds==null?"":item.lower_bounds;
                item.provider.proName=item.provider.proName==null?"":item.provider.proName;
                item.goods_note=item.goods_note==null?"":item.goods_note;

                table.append("<tr><td>" + (i +1)  +
                    "</td><td>" + item.goods_name +
                    "</td><td>" + item.goodsType.type_name +
                    "</td><td>" + item.goods_unit +
                    "</td><td>" + item.goods_totalnum +
                    "</td><td>" + item.goods_price +
                    "</td><td>" + item.goods_purchase_price +
                    "</td><td>" + item.lower_bounds+
                    "</td><td>" + item.provider.proName+
                    "</td><td>" + item.goods_note+
                    "</td></tr>")
            });
        });


        //点击查询按钮
        $("#id_queryProvider").click(function () {
            var name = $("#id_providerText").val();
            deleteAllRowsOfTable("table1");
            $.getJSON("selectAllGoodsByName", {"name": name}, function (res) {
                console.log(res);
                var table = $("#table1");
                $(res.o_list).each(function (i, item) {
                    item.goods_name = item.goods_name==null?"":item.goods_name;
                    item.goodsType.type_name = item.goodsType.type_name==null?"":item.goodsType.type_name;
                    item.goods_unit = item.goods_unit==null?"":item.goods_unit;
                    item.goods_totalnum = item.goods_totalnum==null?"":item.goods_totalnum;
                    item.goods_price=item.goods_price==null?"":item.goods_price;
                    item.goods_purchase_price=item.goods_purchase_price==null?"":item.goods_purchase_price;
                    item.lower_bounds=item.lower_bounds==null?"":item.lower_bounds;
                    item.provider.proName=item.provider.proName==null?"":item.provider.proName;
                    item.goods_note=item.goods_note==null?"":item.goods_note;
                    table.append("<tr><td>" + (i +1)  +
                        "</td><td>" + item.goods_name +
                        "</td><td>" + item.goodsType.type_name +
                        "</td><td>" + item.goods_unit +
                        "</td><td>" + item.goods_totalnum +
                        "</td><td>" + item.goods_price +
                        "</td><td>" + item.goods_purchase_price +
                        "</td><td>" + item.lower_bounds+
                        "</td><td>" + item.provider.proName+
                        "</td><td>" + item.goods_note+
                        "</td></tr>")
                });
            });
        });
    });

</script>
</html>
