<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <title>添加供应商</title>
</head>
<body>

<div>
    <form action="insertGoodsType" name="twForm" id="myform" onsubmit="return addProvider()">
        <table style="margin: 40px auto;">
            <tr>
                <td>类型名称:</td>
                <td><input type="text" name="type_name"></td>
            </tr>
            <tr>
                <td>备注:</td>
                <td><input type="text" name="note"></td>
            </tr>
            <tr>
                <td>父类型:</td>
                <td><input type="text" name="parent"></td>
            </tr>
            <tr>
                <td>级别(级别=父类型级别+1,切勿填错):</td>
                <td><input type="text" name="level"></td>
            </tr>
            <tr>
                <td colspan="1" align="center"><input type="submit" style="margin-top: 5px;" value="添加"></td>
                <td><a href="goodsType">返回</a></td>
            </tr>
        </table>
    </form>
</div>
</body>
<script type="text/javascript">
    function addProvider(){
        var ret = false;
        //判断必填的是否填完 code name password gender age telephone address
        if($("input[name='proName']").val() != "" &&
            $("input[name='proDesc']").val() != "" &&
            $("input[name='proContact']").val() != "" &&
            $("input[name='proPhone']").val() != "" &&
            $("input[name='proAddress']").val() != "" &&
            $("input[name='proFax']").val() != ""
        ){
            ret = true;
        }else {
            alert("请将信息填写完整");
        }
        return ret;
    }
</script>
</html>
