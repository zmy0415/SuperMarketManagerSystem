<%@ page import="com.zmy.pojo.Page" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>修改用户类型</title>
    <%--    <script src="http://www.jq22.com/jquery/jquery-1.10.2.js"/>--%>
</head>
<body>

<div>
    <form action="updateRole" name="twForm" id="myform" onsubmit="return addUser()">
        <table style="margin: 40px auto;">
            <tr style="display: none;">
                <td>id:</td>
                <td><input type="text" name="id" value="${ROLE.id}"></td>
            </tr>
            <tr>
                <td>角色名称:</td>
                <td><input type="text" name="rolename" value="${ROLE.rolename}"></td>
            </tr>
            <tr>
                <td>备注（可空）:</td>
                <td><input type="text" name="note"  value="${ROLE.note}"></td>
            </tr>
            <tr>
                <td>权限:</td>
                <td><input type="text" name="permission"  value="${ROLE.permission}"></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><input type="submit" style="margin-top: 5px;" value="修改"></td>
            </tr>
        </table>

    </form>
</div>
</body>
</html>
