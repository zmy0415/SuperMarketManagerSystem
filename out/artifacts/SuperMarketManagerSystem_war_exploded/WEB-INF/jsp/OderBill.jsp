<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%--<html>--%>
<%--<head>--%>
<%--    <title>Title</title>--%>
<%--</head>--%>
<%--<body>--%>
<%--    进货订单--%>
<%--    <br/>--%>
<%--    //商品外键--%>
<%--    //商品数量--%>
<%--    //总价--%>
<%--    //订单状态--%>
<%--    //确认入库、退货，--%>
<%--</body>--%>
<%--</html>--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    %>
    <base href="<%=basePath%>">
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <script src="http://www.jq22.com/jquery/jquery-1.10.2.js"/>
    <script type="text/javascript" src="<%=basePath%>js/user.js"></script>
    <script type="text/javascript" src="<%=basePath%>js/del.js"></script>
    <link rel="stylesheet" href="<%=basePath%>css/public.css"/>
    <link rel="stylesheet" href="<%=basePath%>css/style.css"/>
    <style type="text/css">
        .providerTable {
            width: 100%;
        }
        .tablebody {
            width: 100%;

            /*//table-layout: fixed;*/
        }
        td{
            white-space: nowrap;  //禁止td换行
            /*//overflow: hidden;  //隐藏X,Y滚动条*/
            text-overflow: ellipsis;//将显示不完的以...显示
        }
        a {
            display:inline-block;
            margin:0px 0px 5px 5px;
            padding:6px 8px;
            font-size:14px;
            outline:none;
            text-align:center;
            /*width:50px;*/
            /*line-height:30px;*/
            cursor: pointer;
        }

        a {
            color:white;
            background-color:rgba(0,64,156,.8);
        }

        a:hover {
            color:white;
            background-color:rgba(255,0,0,.8);
        }
    </style>
</head>

<body>
<div class="search">
    <%--    类型级别：<select id="level-select"></select>--%>
    <%--    商品类型：<select id="example-select"></select>--%>
<%--    <a href="addUserType">添加用户类型</a>--%>
</div>
<%--    //商品外键--%>
<%--    //商品数量--%>
<%--    //总价--%>
<%--    //订单状态--%>
<%--    //确认入库、退货，--%>
<div class="providerTable" style="overflow-x: auto; overflow-y: auto;">
    <div class="tablebodyBox">
        <table class="tablebody" id="table1" cellpadding="0" cellspacing="0">
            <tr class="firstTr">
                <th width="10%">序号</th>
                <th width="10%">订单编号</th>
                <th width="10%">商品名称</th>
<%--                <th width="10%">供应商</th>--%>
                <th width="10%">订单状态</th>
                <th width="10%">数量</th>
<%--                <th width="10%">进价</th>--%>
                <th width="10%">总价</th>
                <th width="5%">创建者</th>
                <th width="10%">创建时间</th>
                <th width="15%">操作</th>
            </tr>
        </table>
    </div>
</div>
</div>
</body>
<script type="text/javascript">

    function deleteAllRowsOfTable(t_id) {
        var table = document.getElementById(t_id);
        var len = table.rows.length;
        for (var i = len - 1; i > 0; i--) {
            table.deleteRow(i);
        }
    }
    function checkEnsure(state,msg){
        if(state == 2 || state==3){
            alert("订单已取消或已完成，不能再次操作");
            return false;
        }
        if (confirm(msg)==true){
            return true;
        }else{
            return false;
        }
    }
    function checkEnsure1(state,msg){
        if(state == 2 || state==3){
            alert("订单已取消或已完成，不能再次操作");
            return false;
        }
        if (confirm(msg)==true){
            return true;
        }else{
            return false;
        }
    }
    //待支付、已支付、已完成、已取消
    var billstateArray = ["待支付","已支付","已完成","已取消"];//0,1,2,3
    //var menu_op = ["<a href='deleteRoleById?id="+item.id+"'  onclick='return checkEnsure(\"是否确认退货\")'>退货</a>"];
    $(document).ready(function () {


        $.getJSON("getAllOderBill", function (res) {
            console.log(res);
            var table = $("#table1");
            $(res.o_list).each(function (i, item) {

                item.note=item.note==null?"":item.note;
                table.append("<tr><td>" + (i +1) +
                    "</td><td>" + item.order_number +
                    "</td><td>" + item.goods.goods_name +
                    /*"</td><td>" + item.provider.proName +*/
                    "</td><td>" + billstateArray[item.order_state] +
                    "</td><td>" + item.goods_num +
                    /*"</td><td>" + item.goods.goods_purchase_price +*/
                    "</td><td>" + item.total_price +
                    "</td><td>" + item.createby +
                    "</td><td>" + item.time +
                    "</td><td>" +
                        "<a href='ensureOrderBill?id="+item.id+"' name='a"+item.order_state+"' onclick=' return checkEnsure("+item.order_state+",\"确认订单之后将会将商品入库\")'>确认订单</a>"+
                        "<a href='cancelOrderBill?id="+item.id+"' name='a"+item.order_state+"' onclick=' return checkEnsure1("+item.order_state+",\"订单取消后将不能再恢复，是否确认取消\")'>取消订单</a>" +
                    "</td></tr>");
                        //订单完成  退货
                //var billstateArray = ["待支付","已支付","已完成","已取消"];//0,1,2,3
                // if(item.order_state ==2){
                //     table.append("<a href='deleteRoleById?id="+item.id+"'  onclick='return checkEnsure(\"是否确认退货\")'>退货</a>");
                // }else if(item.order_state !=3){
                //     table.append( "<a href='updateUserType?id="+item.id+"' onclick=' return checkEnsure(\"确认订单之后将会将商品入库\")'>确认订单</a>");
                // }
                // table.append( "<a href='deleteRoleById?id="+item.id+"'>详情</a>" +
                //     "</td></tr>");
            });
        });

        //点击查询按钮
        $("#id_queryProvider").click(function () {
            var providerName = $("#id_providerText").val();
            deleteAllRowsOfTable("table1");
            $.getJSON("selectProviderByName", {"name": providerName}, function (res) {
                console.log(res);
                item.note=item.note==null?"":item.note;
                var table = $("#table1");
                $(res.o_list).each(function (i, item) {
                    table.append("<tr><td>" + (i +1) +
                        "</td><td>" + item.id +
                        "</td><td>" + item.rolename +
                        "</td><td>" + item.note +
                        "</td><td><a href='ensureOrderBill?id="+item.id+"'>修改</a><a href='deleteRoleById?id="+item.id+"'  onclick='return del()'>删除</a>" +
                        "</td></tr>")
                });
            });
        });
    });


</script>
</html>
