<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
                + path + "/";
    %>
    <base href="<%=basePath%>">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>超市管理系统</title>
    <link rel="stylesheet" href="https://www.jq22.com/jquery/bootstrap-3.3.4.css">
    <link rel="stylesheet" href="https://www.jq22.com/jquery/font-awesome.4.6.0.css">
    <link rel="stylesheet" href="<%=basePath%>css/custom.min.css" />
    <link rel="stylesheet" href="<%=basePath%>font/iconfont.css" type="text/css">
</head>
<style type="text/css">
    /**{
        padding: 0;margin: 0;
    }*/
    body,html{
        width: 100%;
        height: 100%;
    }
    .contentLeft {
        float: left;
        width: 20%;
        height: 100%;
        background:black;
        opacity: 0.7;
    }

    .contentRight {
        float: left;
        width: 80%;
    }

</style>

<body class="nav-md">
<%
    if(request.getSession().getAttribute("User")== null){
        response.sendRedirect(request.getContextPath()+"error/error");
    }
%>
<div class="contentLeft">
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div style="width: 100%">
            <div style="text-align: center">
                <img style="width:100px;height:100px;border-radius:50%;" src="images/login_logo.png"/>
                <h5 style="color: #985f0d">当前用户：${User.name}</h5>
            </div>
        </div>
        <ul class="nav side-menu" id="navmenu"></ul>
        <div style="text-align: center">
            <a href="exitLogin" style="height: 41px;line-height: 41px;padding: 0 21px;background: #02bafa; border: 1px #26bbdb solid;border-radius: 3px;display: inline-block;text-decoration: none;font-size: 12px;outline: none;" >退出登录</a>
        </div>
    </div>
</div>
<div class="contentRight">
    <div class="right_col" role="main" id="rightContent"></div>
</div>

<script src="https://www.jq22.com/jquery/jquery-1.10.2.js"></script>
<script src="https://www.jq22.com/jquery/bootstrap-3.3.4.js"></script>
<script src="js/home.js" type="text/javascript" charset="utf-8"></script>

</body>
<script>

</script>
</html>
