<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    %>
    <base href="<%=basePath%>">
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <script src="http://www.jq22.com/jquery/jquery-1.10.2.js"></script>
    <style>

        td{
            width: 300px;
        }
        a {
            display:inline-block;
            margin:0px 0px 5px 5px;
            padding:6px 8px;
            font-size:14px;
            outline:none;
            text-align:center;
            /*width:50px;*/
            /*line-height:30px;*/
            cursor: pointer;
        }

        a {
            color:white;
            background-color:rgba(0,64,156,.8);
        }

        a:hover {
            color:white;
            background-color:rgba(255,0,0,.8);
        }
    </style>
</head>
<body>


    <table style="margin: 150px auto;">
        <tr>
            <td>进货单号:&nbsp&nbsp<%--<input id="jh_num">--%>${NUMBER}</td>
            <td>商品名称:&nbsp&nbsp<label>${GOODS.goods_name}</label></td>
        </tr>
        <tr>
            <td>供应商:&nbsp&nbsp<label id="gnumber">${GOODS.provider.proName}</label></td>

            <td>数量:&nbsp&nbsp<input type="number" id="totalnum" name="num" min="1" value="1">${GOODS.goods_unit}</td>
        </tr>
        <tr>
            <td>总价:&nbsp&nbsp<label id="allPrice"></label></td>
            <td>时间:&nbsp&nbsp<input type="date" id="createTime"></td>
            <td>创建者:&nbsp&nbsp<label  id="createBy">${User.name}</label></td>
        </tr>
        <tr>
            <td>备注:&nbsp&nbsp
                <input type="text" id="note">
            </td>
        </tr>
        <tr>
            <td>状态:&nbsp&nbsp
                <select id="select_1">
                    <option value="1">已支付</option>
                    <option value="0">未支付</option>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" >
                <button id="tijiao" onclick="upload_tj()">提交</button>
                <button <%--href="stock"--%> id="fanhui" onclick="fanhui()" >返回</button>
            </td>
        </tr>
    </table>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        var time = new Date();
        var day = ("0" + time.getDate()).slice(-2);
        var month = ("0" + (time.getMonth() + 1)).slice(-2);
        var today = time.getFullYear() + "-" + (month) + "-" + (day);

        $('#createTime').val(today);

        $("#totalnum").change(function (){
            var price= $("#totalnum").val()*$("#price")*1;
            $("#totalnum").html(price);

        });
    });

    function fanhui(){
        window.history.back();
    }

    function upload_tj(){
        var dan_hao = '${NUMBER}';
        var goods_id = '${GOODS.goods_id}';
        var total_price = $("#totalnum").val()*'${GOODS.goods_purchase_price}'
        var order_state = $("#select_1").val();
        var createtby = '${User.name}';
        var createtime = $("#createTime").val();
        var note = $("#note").val();

        var data = {
            "order_number" : dan_hao,
            "goods_num" : $("#totalnum").val(),
            "total_price" : total_price,
            "order_state" : order_state,
            "createby" : createtby,
            "time" : createtime,
            "note" : note,
            "goods_id" : goods_id,
            "provider_id":'${GOODS.provider.id}'
        };
        $.post("insertOrderBill",data,function (res){
            window.history.back();
            //alert(res);


        });
        //console.log(dan_hao,goods_id,total_price,order_state,createby,createtime,note);
    }

</script>
</html>
