<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <title>添加供应商</title>
</head>
<body>

    <div>
        <form action="addProvider" name="twForm" id="myform" onsubmit="return addProvider()">
            <table style="margin: 40px auto;">
                <tr>
                    <td>供应商名称:</td>
                    <td><input type="text" name="proName"></td>
                </tr>
                <tr>
                    <td>描述:</td>
                    <td><input type="text" name="proDesc"></td>
                </tr>
                <tr>
                    <td>联系人:</td>
                    <td><input type="text" name="proContact"></td>
                </tr>
                <tr>
                    <td>联系人电话:</td>
                    <td><input type="text" name="proPhone"></td>
                </tr>
                <tr>
                    <td>地址:</td>
                    <td><input type="text" name="proAddress"></td>
                </tr>
                <tr>
                    <td>传真:</td>
                    <td><input type="text" name="proFax"></td>
                </tr>
                <tr>
                    <td colspan="1" align="center"><input type="submit" style="margin-top: 5px;" value="添加"></td>
                    <td><a href="provider">返回</a></td>
                </tr>
            </table>
        </form>
    </div>
</body>
<script type="text/javascript">
    function addProvider(){
        var ret = false;
        //判断必填的是否填完 code name password gender age telephone address
        if($("input[name='proName']").val() != "" &&
            $("input[name='proDesc']").val() != "" &&
            $("input[name='proContact']").val() != "" &&
            $("input[name='proPhone']").val() != "" &&
            $("input[name='proAddress']").val() != "" &&
            $("input[name='proFax']").val() != ""
        ){
            ret = true;
        }else {
            alert("请将信息填写完整");
        }
        return ret;
    }
</script>
</html>
