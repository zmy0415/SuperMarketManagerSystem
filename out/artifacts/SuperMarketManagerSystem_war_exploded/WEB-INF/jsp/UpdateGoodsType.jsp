<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <title>修改商品类型</title>
    <style>
        a {
            display:inline-block;
            margin:0px 0px 5px 5px;
            padding:6px 8px;
            font-size:14px;
            outline:none;
            text-align:center;
            /*width:50px;*/
            /*line-height:30px;*/
            cursor: pointer;
        }

        a {
            color:white;
            background-color:rgba(0,64,156,.8);
        }

        a:hover {
            color:white;
            background-color:rgba(255,0,0,.8);
        }
    </style>
</head>
<body>

<div>
    <form action="updateGoodsType" name="twForm" id="myform" onsubmit="return addProvider()">
        <table style="margin: 40px auto;">
            <tr style="display: none">
                <td>id:</td>
                <td><input type="text" name="type_id" value=${GOODSTYPE.type_id}></td>
            </tr>
            <tr>
                <td>商品类型名称:</td>
                <td><input type="text" name="type_name" value=${GOODSTYPE.type_name}></td>
            </tr>
            <tr>
                <td>商品类型等级:</td>
                <td><input type="text" name="level" value=${GOODSTYPE.level}></td>
            </tr>
            <tr>
                <td>商品类型父类型:</td>
                <td><input type="text" name="parent" value=${GOODSTYPE.parent}></td>
            </tr>
            <tr>
                <td>备注:</td>
                <td><input type="text" name="note" value=${GOODSTYPE.note}></td>
            </tr>
            <tr>
                <td colspan="1" align="center"><input type="submit" style="margin-top: 5px;" value="修改"></td>
                <td><a href="goodsType">返回</a></td>
            </tr>
        </table>
    </form>
</div>
</body>
<script type="text/javascript">
    function addProvider(){
        var ret = false;
        //判断必填的是否填完 code name password gender age telephone address
        if($("input[name='proName']").val() != "" &&
            $("input[name='proDesc']").val() != "" &&
            $("input[name='proContact']").val() != "" &&
            $("input[name='proPhone']").val() != "" &&
            $("input[name='proAddress']").val() != "" &&
            $("input[name='proFax']").val() != ""
        ){
            ret = true;
        }else {
            alert("请将信息填写完整");
        }
        return ret;
    }
</script>
</html>
