package com.zmy.intercepter;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginIntercepter implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        if(session.getAttribute("User")!=null){
            return  true;
        }else{
            response.setCharacterEncoding("utf-8");
            response.sendRedirect(request.getContextPath()+"/error/error.jsp");
            return false;
        }


    }
}
