package com.zmy.controller;

import com.alibaba.fastjson.JSONArray;
import com.zmy.pojo.Goods;
import com.zmy.pojo.GoodsType;
import com.zmy.pojo.Page;
import com.zmy.service.GoodsService;
import com.zmy.utils.autoCreatePojo.PageUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.UUID;

@Controller
public class GoodsController {
    @Resource
    private GoodsService goodsService;

    @RequestMapping(value = "goodsList",produces = {"text/html;charset=utf-8"})
    public String goodsList(){
        return "Goods";
    }

    @RequestMapping(value = "toAddGoods",produces = {"text/html;charset=utf-8"})
    public String toAddGoods(){
        return "AddGoods";
    }

    @RequestMapping(value = "getAllGoods",produces = {"text/html;charset=utf-8"})
    @ResponseBody
    public String getAllGoods(){
        Page page = goodsService.selectAllGoods();
        System.out.println(page);
        return JSONArray.toJSONString(page);
    }
    @RequestMapping(value = "selectGoodsById",produces = {"text/html;charset=utf-8"})
    @ResponseBody
    public String selectGoodsById(@RequestParam("id") int id){
        return JSONArray.toJSONString(goodsService.selectGoodsById(id));
    }
    @RequestMapping(value = "selectAllGoodsByName",produces = {"text/html;charset=utf-8"})
    @ResponseBody
    public String selectAllGoodsByName(@RequestParam("name") String name){
        System.out.println(name);
        return JSONArray.toJSONString(goodsService.selectAllGoodsByName(name));
    }
    @RequestMapping(value = "insertGoods",produces = {"text/html;charset=utf-8"})
    public String insertGoods(Goods goods){
        System.out.println(goods);
        goodsService.insertGoods(goods);
        return "Goods";
    }
    @RequestMapping(value = "ensureGoodsByName",produces = {"text/html;charset=utf-8"})
    @ResponseBody
    public String ensureGoodsByName(@RequestParam("name") String name){

        if(goodsService.ensureGoodsByName(name) != 0){
            return "false";
        }
        return "true";
    }


    @RequestMapping(value = "Goods1")
    public String insertGoods1(){
        return "Goods";
    }


    @RequestMapping(value = "deleteGoodsById",produces = {"text/html;charset=utf-8"})
    public String deleteGoodsById(@RequestParam("id")  int id){
        goodsService.deleteGoodsById(id);
        return "Goods";
    }

    //删除商品
    @RequestMapping(value = "deleteGoodsById1",produces = {"text/html;charset=utf-8"})
    public String deleteGoodsById1(@RequestParam("id")  int id){
        goodsService.deleteGoodsById(id);
        return "Stock";
    }

    @RequestMapping(value = "updateGoods",produces = {"text/html;charset=utf-8"})
    public String updateGoods(Goods goods){
        goodsService.updateGoods(goods);
        return "Goods";
    }

    /*
    goods  type
     */
    //goodsType首页
    @RequestMapping(value = "goodsType",produces = {"text/html;charset=utf-8"})
    public String goodsType(){
        return "GoodsType";
    }

    //添加类型的页面
    @RequestMapping(value = "addGoodsType",produces = {"text/html;charset=utf-8"})
    public String addGoodsType(){
        return "AddGoodsType";
    }

    //修改类型的页面

    @RequestMapping(value = "toUpdateGoodsType",produces = {"text/html;charset=utf-8"})
    public String toUpdateGoodsType(@RequestParam("id") int id,Model model){
        GoodsType goodsType = goodsService.selectGoodsTypeById(id);
        model.addAttribute("GOODSTYPE",goodsType);
        return "UpdateGoodsType";
    }

    @RequestMapping(value = "goodsTypeList",produces = {"text/html;charset=utf-8"})
    @ResponseBody
    public String getAllGoodsType(){
        Page page = goodsService.selectAllGoodsType();
        return JSONArray.toJSONString(page);
    }

    //用 id 查
    @RequestMapping(value = "selectGoodsTypeById",produces = {"text/html;charset=utf-8"})
    @ResponseBody
    public String selectGoodsTypeById(@RequestParam("id") int id){
        Page page = goodsService.selectGoodsByType(id);
        return JSONArray.toJSONString(page);
    }

    @RequestMapping(value = "insertGoodsType",produces = {"text/html;charset=utf-8"})
    public String insertGoodsType(GoodsType goodsType){
        //System.out.println(goodsType.getParent());
        //goodsType.setParent((Integer)goodsType.getParent()==null?0:goodsType.getParent());
        goodsService.insertGoodsType(goodsType);
        return "GoodsType";
    }

    @RequestMapping(value = "deleteGoodsType",produces = {"text/html;charset=utf-8"})
    public String deleteGoodsType(@RequestParam("id") int id){
        try {
            goodsService.deleteGoodsType(id);
        }catch (Exception  e){
            System.out.println(e.getMessage());
            return "GoodsType";
        }
        return "GoodsType";
    }
    @RequestMapping(value = "updateGoodsType",produces = {"text/html;charset=utf-8"})
    public String updateGoodsType(GoodsType goodsType){
        goodsService.updateGoodsType(goodsType);
        return "GoodsType";
    }


    //库存
    @RequestMapping(value = "stock",produces = {"text/html;charset=utf-8"})
    public String stock(GoodsType goodsType){
        return "Stock";
    }


    @RequestMapping(value = "toStockWarning",produces = {"text/html;charset=utf-8"})
    public String toStockWarning(GoodsType goodsType){
        return "StockWarning";
    }

    @RequestMapping(value = "stockWarning",produces = {"text/html;charset=utf-8"})
    @ResponseBody
    public String stockWarning(){
        Page page = goodsService.selectGoodsWarning();
        return JSONArray.toJSONString(page);
    }

     /**
     * 根据UUID生成订单号
     * @return
     */
    private static String getOrderIdByUUId()
    {
        int value = UUID.randomUUID().toString().hashCode();
        if (value < 0) {
            value = -value;
        }
        //0代表前面补充0，10代表长度，d代表正整数
        String orderId = String.format("%010d", value);
        return orderId;
    }
    //进货
    @RequestMapping(value = "toPurchase",produces = {"text/html;charset=utf-8"})
    public String toPurchase(@RequestParam("id") int id, Model model){
         Goods goods = goodsService.selectGoodsById(id);
         model.addAttribute("GOODS",goods);
         //生成进货单号
        String orderIdByUUId = getOrderIdByUUId();
        model.addAttribute("NUMBER",orderIdByUUId);

        return "Purchase";
    }



}
