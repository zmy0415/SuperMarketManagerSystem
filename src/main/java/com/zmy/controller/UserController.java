package com.zmy.controller;

import com.alibaba.fastjson.JSONArray;
import com.zmy.pojo.Page;
import com.zmy.pojo.user.Role;
import com.zmy.pojo.user.User;
import com.zmy.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.*;

@Controller
public class UserController {

    @Resource
    private UserService userService;

    /*
    登录校验
     */
    @RequestMapping(value = "home", method = RequestMethod.POST,produces = {"text/html;charset=utf-8"})
    public String login(@RequestParam("username") String username,
                        @RequestParam("password") String pwd,
                        HttpServletRequest request
    ) {

        //根据code和pwd查
        User user = new User();
        user.setCode(username);
        user.setPassword(pwd);
        User res = userService.checkUserForLogin(user);

        //System.out.println(res+"--------------------"+username+"--"+pwd);
        if (res != null) {
            request.getSession().setAttribute("User",res);
            return "redirect:SupermarketManagerSystem";
        }
        return "redirect:index.jsp";
    }

    //登录中间校验
    @RequestMapping(value = "SupermarketManagerSystem",produces = {"text/html;charset=utf-8"})
    public String check(HttpServletRequest request,Model model){
        Object user = request.getSession().getAttribute("User");
        if(user == null){
            model.addAttribute("error_msg","退出登录之后，不能直接返回，请重新登录");
            return "error/error";
        }
        return "home2";
    }


    //退出登录
    @RequestMapping(value = "exitLogin",produces = {"text/html;charset=utf-8"})
    public String exitLogin(HttpServletRequest request,HttpServletResponse response){
        HttpSession session = request.getSession();
        session.removeAttribute("User");
        session.invalidate();
        Cookie cookie = new Cookie("User",null);
        cookie.setMaxAge(0);
        cookie.setPath(request.getContextPath());
        response.addCookie(cookie);
        return "redirect:index.jsp";
    }


    /*
    获取所有的用户
    返回 page 对象 json
     */
    @RequestMapping(value = "userList",produces = {"text/html;charset=utf-8"})
    //@ResponseBody
    public ModelAndView getUserList(HttpServletResponse resp) {
        Page page = userService.selectAllUser();
        ModelAndView mv = new ModelAndView();
        mv.setViewName("User");
        mv.addObject("PageBean",page);
        return mv;
    }
    /*
    获取所有的用户
    返回 page 对象 json
     */
    @RequestMapping(value = "queryUserList",produces = {"text/html;charset=utf-8"})
    @ResponseBody
    public String queryUserList() {

        Page page = userService.selectAllUser();

        return JSONArray.toJSONString(page);
    }
    /*
    通过Id查询
    返回用户实体对象 json
     */

    @RequestMapping(value = "/getUserById",produces = {"text/html;charset=utf-8"})
    @ResponseBody
    public String getUserById(@RequestParam("id") int id){
        User user = userService.selectUserById(id);
        //return JSONArray.toJSONString(user);
        System.out.println(id+"  "+user);
        return null;
    }


    /*
    根据名字查询用户
    返回 page 对象 json
     */
    @RequestMapping(value = "/queryUser",produces = {"text/html;charset=utf-8"})
    @ResponseBody
    public String user(@RequestParam(value = "name") String name){//,required = false)
        User user = new User();
        user.setName(name);
        System.out.println(user);
        Page page = userService.selectUsersByName(user);
        return JSONArray.toJSONString(page);
    }

    /*
    添加用户
    获取用户实体对象
    返回 "true" or "false"
     */
    @RequestMapping(value = "addUser",produces = {"text/html;charset=utf-8"})
    //@ResponseBody
    public String addUser(User user){
        //System.out.println(user);
        int res = userService.insertUser(user);
        return "User";
    }

    /*
    更新用户数据
    获取用户实体对象
    返回 "true" or "false"
     */
    @RequestMapping(value = "updateUser",produces = {"text/html;charset=utf-8"})
    public String updateUser(User user){
        int ret = userService.updateUser(user.getId(),user);
        return "User";
    }

    /*
    删除用户
    通过id删
    //获取用户实体对象
    返回 "true" or "false"
     */
    @RequestMapping(value = "deleteUser",produces = {"text/html;charset=utf-8"})
    //@ResponseBody
    public String deleteUser(@RequestParam("id") int id){
        User user = new User();
        user.setId(id);
        int ret = userService.deleteUser(user);
        return "User";
    }

    /*
    主页点击切换iframe 的 src
    参数：需要切换的
     */
    @RequestMapping("changeCoreFrame")
    public String changeCoreFrame(@RequestParam("url") String url){
        System.out.println(url);
        return url;
    }

    /*
    跳转到用户数据修改界面
     */
    @RequestMapping(value = "toUpdateUser",produces = {"text/html;charset=utf-8"})
    public String toUpdateUser(@RequestParam("id") int id, Model model){
        User user = userService.selectUserById(id);
        user.setId(id);
        model.addAttribute("USER",user);
        return "UpdateUser";
    }




    //userType
    //userType
    @RequestMapping(value = "userType",produces = {"text/html;charset=utf-8"})
    public String userType(){
        return "UserType";
    }


    @RequestMapping(value = "addUserType",produces = {"text/html;charset=utf-8"})
    public String addUserType(){
        return "AddUserType";
    }

    @RequestMapping(value = "updateUserType",produces = {"text/html;charset=utf-8"})
    public String updateUserType(int id,Model model){
        Role role = userService.selectRoleById(id);
        model.addAttribute("ROLE",role);
        return "UpdateUserType";
    }


    @RequestMapping(value = "selectAllRole",produces = {"text/html;charset=utf-8"})
    @ResponseBody
    public String selectAllRole(){
        Page page = userService.selectAllRole();
        return JSONArray.toJSONString(page);
    }
    @RequestMapping(value = "selectUsersByRoleId",produces = {"text/html;charset=utf-8"})
    @ResponseBody
    public String selectUsersByRoleId(int id){
        Page page = userService.selectUsersByRoleId(id);
        return JSONArray.toJSONString(page);
    }
//    public String selectRoleById(int id){
//        return null;
//    }
    @RequestMapping(value = "insertRole",produces = {"text/html;charset=utf-8"})
    public String insertRole(Role role){
        //System.out.println(role);
        userService.insertRole(role);
        return "UserType";
    }
    @RequestMapping(value = "deleteRoleById",produces = {"text/html;charset=utf-8"})
    public String deleteRoleById(int id){
        try{
            userService.deleteRoleById(id);
        }catch (Exception e){
            return "UserType";
        }
        return "UserType";
    }
    @RequestMapping(value = "updateRole",produces = {"text/html;charset=utf-8"})
    public String updateRole(Role role){
        System.out.println(role);
        try{
            userService.updateRole(role);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        return "UserType";
    }

}
