package com.zmy.controller;

import com.alibaba.fastjson.JSONArray;
import com.zmy.pojo.Page;
import com.zmy.pojo.bill.BillList;
import com.zmy.service.BillListService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class BillController {

    @Resource
    private BillListService billListService;
    @RequestMapping(value = "oderBill",produces = {"text/html;charset=utf-8"})
    public String oderBill(){
        return "OderBill";
    }
    @RequestMapping(value = "getAllOderBill",produces = {"text/html;charset=utf-8"})
    @ResponseBody
    public  String getAllOderBill(){
        Page page = billListService.selectAllBillList();
        System.out.println(page);
        return JSONArray.toJSONString(page);
    }

    @RequestMapping(value = "insertOrderBill",produces = {"text/html;charset=utf-8"})
    @ResponseBody
    public String insertOrderBill(BillList billList){
        //System.out.println(billList);
        //System.out.println(billList.getTime());
        DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = format1.parse(billList.getTime());
            //System.out.println(date);
            billList.setCreatetime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(billList);
        billListService.insertBillList(billList);
        return "";
    }
    @RequestMapping(value = "ensureOrderBill",produces = {"text/html;charset=utf-8"})
    public String ensureOrderBill(int id){
        //获取订单
        BillList billList = billListService.selectBillListById(id);
        System.out.println("bill--:"+billList);
        //如果订单的状态为
        if(billList.getOrder_state() != 3 && billList.getOrder_state() !=2){
            billListService.insertBillListGoods(billList);
        }

        //billListService.insertBillListGoods()
        return "OderBill";
    }


    //cancelOrderBill
    @RequestMapping(value = "cancelOrderBill",produces = {"text/html;charset=utf-8"})
    public String cancelOrderBill(@RequestParam("id") int id){
        billListService.cancelOrderBill(id);
        return "OderBill";
    }

}
