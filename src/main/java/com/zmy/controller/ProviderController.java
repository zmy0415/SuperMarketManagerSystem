package com.zmy.controller;


import com.alibaba.fastjson.JSONArray;
import com.zmy.pojo.Page;
import com.zmy.pojo.Provider;
import com.zmy.service.ProviderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

@Controller
public class ProviderController {

    @Resource
    private ProviderService providerService;
    /*
    供应商的首页
    进入首页后自动查询获取所有的供应商
     */
    @RequestMapping("/provider")
    public ModelAndView provider(){
        ModelAndView mv = new ModelAndView();
        Page page = providerService.selectAllProvider();
        mv.addObject("PageBean",page);
        mv.setViewName("Provider");
        return mv;
    }
    /*
    查询所有的供应商
     */
    @RequestMapping(value = "/selectAllProvider",produces = {"text/html;charset=utf-8"})
    @ResponseBody
    public String selectAllProvider(){
        Page page = providerService.selectAllProvider();
        return JSONArray.toJSONString(page);
    }
    /*
    根据名字查询
     */
    @RequestMapping(value = "/selectProviderByName",produces = {"text/html;charset=utf-8"})
    @ResponseBody
    public String selectProviderByName(@RequestParam("name") String name){
        Page page = providerService.selectProviderByName(name);
        System.out.println(name);
        return JSONArray.toJSONString(page);
    }

    /*
    跳转到添加供应商页面
     */
    @RequestMapping(value = "/toAddProvider",produces = {"text/html;charset=utf-8"})
    public String toAddProvider(){
        return "AddProvider";
    }


    /*
    添加供应商
     */
    @RequestMapping("/addProvider")
    //@ResponseBody
    public String addProvider(Provider provider){
        //System.out.println("add");
        int res = providerService.insertProvider(provider);
        return "Provider";
    }
    /*
    更新供应商数据
     */
    @RequestMapping("updateProvider")
    //@ResponseBody
    public String updateProvider(Provider provider){
        int res = providerService.updateProvider(provider);
        return "Provider";
    }

    /*
    删除供应商
     */
    @RequestMapping("deleteProvider")
    public String deleteProvider(@RequestParam("id") int id){
        Provider provider = new Provider();
        provider.setId(id);
        int res = providerService.deleteProvider(provider);
        return "Provider";
    }


    @RequestMapping(value = "toUpdateProvider" ,produces = {"text/html;charset=utf-8"} )
    public String toUpdateProvider(@RequestParam("id") int id, Model model){
        System.out.println(id);
        Provider provider = providerService.selectProviderById(id);
        model.addAttribute("PROVIDER",provider);
        return "UpdateProvider";
    }

}
