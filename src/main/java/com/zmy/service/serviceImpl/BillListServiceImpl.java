package com.zmy.service.serviceImpl;

import com.zmy.dao.BillMapper;
import com.zmy.pojo.bill.BillList;
import com.zmy.pojo.Page;
import com.zmy.pojo.Provider;
import com.zmy.service.BillListService;
import com.zmy.utils.autoCreatePojo.PageUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.List;


@Service("billListService")
public class BillListServiceImpl implements BillListService {

    @Resource
    private BillMapper mapper;

    @Override
    public Page selectAllBillList() {
        System.out.println(11111);
        List<BillList> billLists = mapper.selectAllBillList();
        System.out.println(billLists);
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
        for (BillList billList : billLists) {
            String dateString = sd.format(billList.getCreatetime());
            System.out.println(dateString);
            billList.setTime(dateString);
        }
        return PageUtils.getPage(billLists);
    }

    @Override
    public Page selectBillListByName(String name) {
        return null;
    }

    @Override
    public Page selectBillListByProvider(Provider provider) {
        return null;
    }

    @Override
    public BillList selectBillListById(int id) {
        return mapper.selectBillListById(id);
    }

    @Override
    public int insertBillList(BillList billList) {
        return mapper.insertBillList(billList);
    }

    @Override
    public int insertBillListGoods(BillList billList) {
        //修改该商品的数量就可以了
        return mapper.ensureBill(billList);
    }

    @Override
    public int cancelOrderBill(int id) {
        return mapper.cancelOrderBill(id);
    }

    @Override
    public int updateBillList(int id, BillList billList) {
        return 0;
    }

    @Override
    public int deleteBillList(BillList billList) {
        return 0;
    }
}
