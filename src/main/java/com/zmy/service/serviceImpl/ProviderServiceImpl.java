package com.zmy.service.serviceImpl;

import com.zmy.dao.ProviderMapper;
import com.zmy.pojo.Page;
import com.zmy.pojo.Provider;
import com.zmy.pojo.user.User;
import com.zmy.service.ProviderService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("providerService")
public class ProviderServiceImpl implements ProviderService {

    @Resource
    private ProviderMapper mapper;


    private Page getPage(List<Provider> providers) {
        Page page = new Page();
        page.setO_list(providers);
        page.setPageSize(10);
        page.setTotalRows(providers.size());
        page.setCurrentPage(0);
        page.setTotalPage(providers.size()/10+1);
        return page;
    }
    
    @Override
    public Page selectAllProvider() {
        List<Provider> providers = mapper.selectAllProviders();
        Page page = getPage(providers);
        return page;
    }

    @Override
    public Page selectProviderByName(String name) {
        return getPage(mapper.selectProviderByName(name));
    }

    @Override
    public Provider selectProviderById(int id) {
        return mapper.selectProviderById(id);
    }

    @Override
    public int insertProvider(Provider provider) {
        return mapper.addProvider(provider);
    }

    @Override
    public int updateProvider(Provider provider) {
        return mapper.updateProviderById(provider);
    }

    @Override
    public int deleteProvider(Provider provider) {
        return mapper.deleteProviderById(provider.getId());
    }
}
