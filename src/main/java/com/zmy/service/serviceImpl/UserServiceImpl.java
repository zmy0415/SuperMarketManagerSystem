package com.zmy.service.serviceImpl;

import com.zmy.dao.UserMapper;
import com.zmy.pojo.Page;
import com.zmy.pojo.user.Role;
import com.zmy.pojo.user.User;
import com.zmy.service.UserService;
import com.zmy.utils.autoCreatePojo.PageUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper mapping;

    @Override
    public User checkUserForLogin(User user) {
        User res = mapping.checkUserForLogin(user);
        //System.out.println(res);
        return res;
    }

    @Override
    public Page selectAllUser() {
        List<User> users = mapping.selectAllUser();
        return PageUtils.getPage(users);
    }

    @Override
    public User selectUserById(int id) {
        User user = mapping.getUserById(id);
        return user;
    }



    @Override
    public Page selectUserByName(String name) {
        return null;
    }

    @Override
    public Page selectUsersByName(User user) {
        List<User> users = mapping.selectUsersByName(user);
        System.out.println(users);
        return PageUtils.getPage(users);
    }

    @Override
    public int insertUser(User user) {
        int i = mapping.insertUser(user);
        return i;
    }

    @Override
    public int updateUser(int id, User user) {
        int i =  mapping.updateUser(user);
        System.out.println(i);
        return i;
    }

    @Override
    public int deleteUser(User user) {
        return mapping.deleteUser(user);
    }








    //role
    @Override
    public Page selectAllRole() {
        return PageUtils.getPage(mapping.selectAllRole());
    }

    @Override
    public Page selectUsersByRoleId(int id) {
        return PageUtils.getPage(mapping.selectUsersByRoleId(id));
    }

    @Override
    public Role selectRoleById(int id) {
        return mapping.selectRoleById(id);
    }

    @Override
    public int insertRole(Role role) {
        return mapping.insertRole(role);
    }

    @Override
    public int deleteRoleById(int id) {
        return mapping.deleteRoleById(id);
    }

    @Override
    public int updateRole(Role role) {
        return mapping.updateRole(role);
    }


}
