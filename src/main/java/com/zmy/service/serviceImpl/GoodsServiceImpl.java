package com.zmy.service.serviceImpl;

import com.zmy.dao.GoodsMapper;
import com.zmy.pojo.Goods;
import com.zmy.pojo.GoodsType;
import com.zmy.pojo.Page;
import com.zmy.pojo.Provider;
import com.zmy.service.GoodsService;
import com.zmy.utils.autoCreatePojo.PageUtils;
import org.apache.ibatis.javassist.bytecode.stackmap.TypeData;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("goodsService")
public class GoodsServiceImpl implements GoodsService {

    @Resource
    private GoodsMapper mapper;

    @Override
    public Page selectAllGoods() {
        System.out.println(mapper.selectAllGoods());
        return PageUtils.getPage(mapper.selectAllGoods());
    }

    @Override
    public Goods selectGoodsById(int id) {
        return mapper.selectGoodsById(id);
    }

    @Override
    public Page selectAllGoodsByName(String name) {
        System.out.println(mapper.selectAllGoodsByName(name));
        return PageUtils.getPage(mapper.selectAllGoodsByName(name));
    }

    @Override
    public int insertGoods(Goods goods) {
        System.out.println("----"+goods);
        return mapper.insertGoods(goods);
    }

    @Override
    public int deleteGoodsById(int id) {
        return mapper.deleteGoodsById(id);
    }


    @Override
    public int updateGoods(Goods goods) {
        return mapper.updateGoods(goods);
    }

    @Override
    public int ensureGoodsByName(String name) {
        int res = mapper.ensureGoodsByName(name);

        return res;
    }

    @Override
    public Page selectAllGoodsType() {
        return PageUtils.getPage(mapper.selectAllGoodsType());
    }

    @Override
    public Page selectALlGoodsByType(int id) {
        return PageUtils.getPage(mapper.selectALlGoodsByType(id));
    }

    @Override
    public Page selectGoodsByName(String name) {
        return null;
    }

    @Override
    public Page selectGoodsByType(int type_id) {
        return PageUtils.getPage(mapper.selectGoodsByType(type_id));
    }

    @Override
    public GoodsType selectGoodsTypeById(int id) {
        return mapper.selectGoodsTypeById(id);
    }

    @Override
    public Page selectGoodsWarning() {
        return PageUtils.getPage(mapper.selectGoodsWarning());
    }

    @Override
    public int insertGoodsType(GoodsType goodsType) {
        return mapper.insertGoodsType(goodsType);
    }

    @Override
    public int deleteGoodsType(int type_id) {
        return mapper.deleteGoodsType(type_id);
    }

    @Override
    public int updateGoodsType(GoodsType goodsType) {
        return mapper.updateGoodsType(goodsType);
    }
}
