package com.zmy.service;

import com.zmy.pojo.Page;
import com.zmy.pojo.user.Role;
import com.zmy.pojo.user.User;

public interface UserService {

    User checkUserForLogin(User user);

    Page selectAllUser();
    User selectUserById(int id);
    Page selectUserByName(String name);
    Page selectUsersByName(User user);
    int insertUser(User user);
    //只要用户存在他的id就是唯一且不变的
    int updateUser(int id,User user);
    int deleteUser(User user);

    //role
    Page selectAllRole();
    Page selectUsersByRoleId(int id);
    Role selectRoleById(int id);
    int insertRole(Role role);
    int deleteRoleById(int id);
    int updateRole(Role role);
}
