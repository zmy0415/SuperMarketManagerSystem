package com.zmy.service;

import com.zmy.pojo.bill.BillList;
import com.zmy.pojo.Page;
import com.zmy.pojo.Provider;

public interface BillListService {
    Page selectAllBillList();
    Page selectBillListByName(String name);
    Page selectBillListByProvider(Provider provider);
    BillList selectBillListById(int id);

    int insertBillList(BillList billList);
    //确认订单，一键入库
    int insertBillListGoods(BillList billList);

    //取消订单
    int cancelOrderBill(int id);

    int updateBillList(int id,BillList billList);
    int deleteBillList(BillList billList);

}
