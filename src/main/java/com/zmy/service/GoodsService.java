package com.zmy.service;

import com.zmy.pojo.Goods;
import com.zmy.pojo.GoodsType;
import com.zmy.pojo.Page;

public interface GoodsService {
    Page selectAllGoods();
    Goods selectGoodsById(int id);
    Page selectAllGoodsByName(String name);

    int insertGoods(Goods goods);

    int deleteGoodsById(int id);

    int updateGoods(Goods goods);

    int ensureGoodsByName(String name);

    /*
    type
     */
    Page selectAllGoodsType();
    Page selectALlGoodsByType(int id);
    Page selectGoodsByName(String name);
    Page selectGoodsByType(int type_id);
    GoodsType selectGoodsTypeById(int id);

    //库存预警
    Page selectGoodsWarning();

    int insertGoodsType(GoodsType goodsType);
    int deleteGoodsType(int type_id);
    int updateGoodsType(GoodsType goodsType);



}
