package com.zmy.service;

import com.zmy.pojo.Page;
import com.zmy.pojo.Provider;

public interface ProviderService {

    Page selectAllProvider();
    Page selectProviderByName(String name);

    Provider selectProviderById(int id);

    int insertProvider(Provider provider);
    //只要用户存在他的id就是唯一且不变的
    int updateProvider(Provider provider);
    int deleteProvider(Provider provider);
}
