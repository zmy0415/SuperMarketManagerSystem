package com.zmy.utils.autoCreatePojo;

import com.zmy.pojo.Page;

import java.util.List;

public class PageUtils {
    public static Page getPage(List<?> users) {
        Page page = new Page();
        page.setO_list(users);
        page.setPageSize(10);
        page.setTotalRows(users.size());
        page.setCurrentPage(0);
        page.setTotalPage(users.size()/10+1);
        return page;
    }
}
