package com.zmy.utils.autoCreatePojo;

public enum BillState {
    //待支付、已支付、已完成、已取消
    S_WAIT_PAID(1,"待支付"),
    S_PAID(2,"已支付"),
    S_FINISH(3,"已完成"),
    S_CANCELLED(4,"已取消");
    private int id;
    private String text;
    private BillState(int id,String text){
        this.id = id;
        this.text = text;
    }

    @Override
    public String toString() {
        return this.text;
    }
}
