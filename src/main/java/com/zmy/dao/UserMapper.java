package com.zmy.dao;

import com.zmy.pojo.user.Role;
import com.zmy.pojo.user.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userMapping")
public interface UserMapper {
    User getUserById(int id);
    User checkUserForLogin(User user);

    List<User> selectAllUser();
    List<User> selectUsersByName(User user);//模糊查
    int insertUser(User user);
    int deleteUser(User user);
    int updateUser(User user);


    List<Role> selectAllRole();
    List<User> selectUsersByRoleId(int id);
    Role selectRoleById(int id);

    int insertRole(Role role);
    int deleteRoleById(int id);
    int updateRole(Role role);

}
