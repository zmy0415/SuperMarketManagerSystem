package com.zmy.dao;
import com.zmy.pojo.Provider;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository("providerMapper")
public interface ProviderMapper {
    List<Provider> selectAllProviders();
    List<Provider> selectProviderByName(String name);
    Provider selectProviderById(int id);
    int addProvider(Provider provider);
    int deleteProviderById(int id);
    int updateProviderById(Provider provider);
}
