package com.zmy.dao;

import com.zmy.pojo.Goods;
import com.zmy.pojo.GoodsType;
import com.zmy.pojo.Page;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("goodsMapper")
public interface GoodsMapper {
    List<Goods> selectAllGoods();
    Goods selectGoodsById(int id);
    List<Goods> selectAllGoodsByName(String name);

    int insertGoods(Goods goods);

    int deleteGoodsById(int id);

    int updateGoods(Goods goods);



    //goodsType
    List<GoodsType> selectAllGoodsType();
    List<GoodsType> selectALlGoodsByType(int type_id);
    List<Goods> selectGoodsByType(int type_id);
    GoodsType selectGoodsTypeById(int id);
    int ensureGoodsByName(String name);
    int insertGoodsType(GoodsType goodsType);
    int deleteGoodsType(int id);
    //删除类及其子类
    int deleteALlGoodsByType(int type_id);
    int updateGoodsType(GoodsType goodsType);

    //selectGoodsWarning  库存预警
    List<Goods> selectGoodsWarning();
}
