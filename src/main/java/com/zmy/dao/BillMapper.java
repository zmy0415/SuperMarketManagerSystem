package com.zmy.dao;

import com.zmy.pojo.bill.BillList;
import java.util.List;
public interface BillMapper {
    //进货订单
    List<BillList> selectAllBillList();
    List<BillList> selectBillListByProviderId(int id);
    List<BillList> selectBillListByGoodsId(int id);
    BillList selectBillListById(int id);
    int insertBillList(BillList billList);
    //修改订单的状态
    int updateBillState(BillList billList);
    //确认订单 确认之后会将订单中的商品入库
    int ensureBill(BillList billList);

    //取消订单
    int  cancelOrderBill(int id);

    int deleteBillById(int id);


}
