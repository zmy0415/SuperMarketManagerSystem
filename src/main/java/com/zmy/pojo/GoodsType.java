package com.zmy.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodsType {
    private int type_id;
    private String type_name;
    private String note;
    private Integer parent;
    private int level;
}
