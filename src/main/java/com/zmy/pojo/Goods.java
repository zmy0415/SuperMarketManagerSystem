package com.zmy.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Goods {

    private long goods_id;
    private String goods_name;
    private int fk_provider;
    private int fk_goods_kind;

    private Provider provider;
    private GoodsType goodsType;

    private double goods_price;
    private double goods_purchase_price;
    private String goods_note;
    private String goods_pic;
    private int goods_totalnum;
    private String goods_unit;
    private long lower_bounds;
}









