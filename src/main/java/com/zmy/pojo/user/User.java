package com.zmy.pojo.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private int id;
    private String code;
    private String name;
    private String password;
    private String role_array;
    private String address;
    private String telephone;
    private int gender;
    private int age;
    private int role_id;

    private Role role;
}
