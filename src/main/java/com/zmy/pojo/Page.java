package com.zmy.pojo;


import lombok.Data;
import lombok.ToString;

import java.util.List;



@Data
@ToString
public class Page{
    private int totalPage;
    private int currentPage;
    private int totalRows;
    private int pageSize;
    private List o_list;
}
