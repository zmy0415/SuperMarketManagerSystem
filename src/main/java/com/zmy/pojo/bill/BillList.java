package com.zmy.pojo.bill;

import com.zmy.pojo.Goods;
import com.zmy.pojo.Provider;
import com.zmy.utils.autoCreatePojo.BillState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

//每种商品生成一个订单  进货
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BillList {
    //进货订单号
    private long id;
    private long goods_id;
    private long provider_id;
    private String order_number;
    private int goods_num;
    private double total_price;
    private int order_state;
    private String createby;
    private Date createtime;
    private String note;

    private Goods goods;
    private Provider provider;


    private String time;
    private BillState billState;

    //商品外键
    //商品数量
    //总价
    //订单状态
    //一键入库
}
