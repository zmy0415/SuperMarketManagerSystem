<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    %>
    <base href="<%=basePath%>">
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <script src="http://www.jq22.com/jquery/jquery-1.10.2.js"/>
    <script type="text/javascript" src="<%=basePath%>js/user.js"></script>
    <script type="text/javascript" src="<%=basePath%>js/del.js"></script>
    <link rel="stylesheet" href="<%=basePath%>css/public.css"/>
    <link rel="stylesheet" href="<%=basePath%>css/style.css"/>
    <style type="text/css">
        .providerTable {
            width: 100%;
        }

        .tablebody {
            width: 100%;
        }
        a {
            display:inline-block;
            margin:0px 0px 5px 5px;
            padding:6px 8px;
            font-size:14px;
            outline:none;
            text-align:center;
            /*width:50px;*/
            /*line-height:30px;*/
            cursor: pointer;
        }

        a {
            color:white;
            background-color:rgba(0,64,156,.8);
        }

        a:hover {
            color:white;
            background-color:rgba(255,0,0,.8);
        }
    </style>
</head>


<body>
<div class="search">
    <span>供应商名称：</span>
    <input type="text" id="id_providerText" placeholder="请输入供应商的名称"/>
    <input type="button" value="查询" id="id_queryProvider"/>
    <a href="toAddProvider">添加供应商</a>

</div>
<!--供应商操作表格-->
<div class="providerTable" style="overflow-x: auto; overflow-y: auto;">
    <div class="tablebodyBox">
        <table class="tablebody" id="table1" cellpadding="0" cellspacing="0">
            <tr class="firstTr">
                <th width="10%">供应商编号</th>
                <th width="20%">供应商名称</th>
                <th width="10%">联系人</th>
                <th width="10%">联系电话</th>
                <th width="10%">传真</th>
                <th width="10%">地址</th>
                <th width="15%">描述</th>
                <th width="15%">操作</th>
            </tr>
        </table>
    </div>
</div>
</div>
</body>
<script type="text/javascript">

    function deleteAllRowsOfTable(t_id) {
        var table = document.getElementById(t_id);
        var len = table.rows.length;
        for (var i = len - 1; i > 0; i--) {
            table.deleteRow(i);
        }
    }

    $(document).ready(function () {
        $.getJSON("selectAllProvider", function (res) {
            console.log(res);
            var table = $("#table1");
            $(res.o_list).each(function (i, item) {
                table.append("<tr><td>" + (i +1) +
                    "</td><td>" + item.proName +
                    "</td><td>" + item.proContact +
                    "</td><td>" + item.proPhone +
                    "</td><td>" + item.proFax +
                    "</td><td>" + item.proAddress +
                    "</td><td>" + item.proDesc +
                    "</td><td><a href='toUpdateProvider?id="+item.id+"'>修改</a><a href='deleteProvider?id="+item.id+"'  onclick='return del()'>删除</a>" +
                    "</td></tr>")
            });
        });


        //点击查询按钮
        $("#id_queryProvider").click(function () {
            var providerName = $("#id_providerText").val();
            deleteAllRowsOfTable("table1");
            $.getJSON("selectProviderByName", {"name": providerName}, function (res) {
                console.log(res);
                var table = $("#table1");
                $(res.o_list).each(function (i, item) {
                    table.append("<tr><td>" + (i +1)+
                        "</td><td>" + item.proName +
                        "</td><td>" + item.proContact +
                        "</td><td>" + item.proPhone +
                        "</td><td>" + item.proFax +
                        "</td><td>" + item.proAddress +
                        "</td><td>" + item.proDesc +
                        "</td><td><a href='toUpdateProvider?id="+item.id+"'>修改</a><a href='deleteProvider?id="+item.id+"'  onclick='return del()'>删除</a>" +
                        "</td></tr>")
                });
            });
        });
    });


</script>
</html>
