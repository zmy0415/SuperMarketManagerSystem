<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    %>
    <base href="<%=basePath%>">
    <meta http-equiv="content-type" content="text/html;charset=utf-8">

<%--    <script src="http://www.jq22.com/jquery/jquery-1.10.2.js"/>--%>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <script src="<%=basePath%>js/bootstrap.js"></script>
<%--    <script type="text/javascript" src="<%=basePath%>js/user.js"></script>--%>
    <script type="text/javascript" src="<%=basePath%>js/del.js"></script>
    <link rel="stylesheet" href="<%=basePath%>css/public.css"/>
    <link rel="stylesheet" href="<%=basePath%>css/style.css"/>
    <style>
        tr{
            background-color: #d6dfe6;
        }
        * {
            margin:0;
            padding:0;
        }
        #cover {
            background-color:black;
            position:absolute;
            left:0px;
            top:0px;
            width:100%;
            height:100%;
            filter:alpha(opacity=30);
            opacity:0.3;
            display:none;
            z-index:3;
        }
        #showdiv {
            width:600px;
            margin:0 auto;
            height:400px;
            display:none;
            position:absolute;
            top:10%;
            left:10%;
            z-index:3;
            background:#fff;
            display:block;
            border:3px solid #465263;
        }
        #showdiv .center-box1 {
            background:#F8F7F7;
            width:100%;
            height:100px;
            text-align:center;
        }
        #showdiv .center-box2 {
            text-indent:50px;
            width:600px;
            height:276px;
            background-color:#ffffff;
            display:block;
        }
        #showdiv .center-box2 p {
            font-size:24px;
            color:black;
            position:absolute;
            top:17%;
            left:20%;
            font-weight:800;
        }
        #showdiv .center-box2 span {
            font-size:17px;
            position:absolute;
            top:31%;
            left:20%;
            font-weight:900;
            text-decoration:none;
            color:red;
        }
        #showdiv .center-box2 a {
            color:#115490;
            font-size:17px;
            position:absolute;
            top:53%;
            left:26%;
            font-weight:900;
        }
        #showdiv .center-box2 a:hover {
            color:#115490;
        }
        #showdiv .center-box3 {
            width:50px;
            height:33px;
            text-align:center;
            color:#adadad;
            font-size:26px;
            float:right;
            cursor:pointer;
        }
        .center {
            width:100%;
            height:50px;
            margin-top:25px;
            position:relative;
        }
        .center input {
            position:absolute;
            top:45px;
            left:20px;
        }
        .center span {
            position:absolute;
            left:34px;
            top:41px;
            font-size:12px;
        }
        .center p {
            display:inline-block;
            width:62px;
            height:30px;
            background-color:#A9A9A9;
            position:absolute;
            top:38px;
            right:41px;
            line-height:26px;
            color:white;
        }
        #btn_1{
            text-align: center;
            width: 120px;
            padding-left:40px;
            float: right;
            margin: 10px 60px;
            height: 30px;
            line-height: 30px;
            border: 1px solid #0c89de;
            border-radius: 4px;
            color: #fff;
            font-weight: bold;
            /*background:#47acf1 url("../images/tianjia.png") 10px center no-repeat;*/
            display: inline-block;
        }
        th,td{
            text-align: center;
        }
        a {
            display:inline-block;
            margin:0px 0px 5px 5px;
            padding:6px 8px;
            font-size:14px;
            outline:none;
            text-align:center;
            /*width:50px;*/
            /*line-height:30px;*/
            cursor: pointer;
        }

        a {
            color:white;
            background-color:rgba(0,64,156,.8);
        }

        a:hover {
            color:white;
            background-color:rgba(255,0,0,.8);
        }
    </style>
</head>


<body>
<div class="search">
    <span>用户名：</span>
    <input type="text" id="username" placeholder="请输入用户名"/>
    <input type="button" id="queryByName" value="查询" style="text-align: center"/>


    <button id="btn_1" onclick="showWindow('js弹窗 js弹出DIV,并使整个页面背景变暗')">添加用户</button>

    <div id="cover"></div>
    <div id="showdiv">
        <div class="center-box3" onclick="closeWindow()">x</div>

        <form action="../addUser" name="twForm" id="myform" onsubmit="return addUser()">
            <table style="margin: 40px auto;">
                <tr>
                    <td>帐号(英文):</td>
                    <td><input type="text" name="code"></td>
                </tr>
                <tr>
                    <td>用户名称:</td>
                    <td><input type="text" name="name"></td>
                </tr>
                <tr>
                    <td>密码:</td>
                    <td><input type="text" name="password"></td>
                </tr>
                <tr>
                    <td>地址:</td>
                    <td><input type="text" name="address"></td>
                </tr>
                <tr>
                    <td>电话:</td>
                    <td><input type="text" name="telephone"></td>
                </tr>
                <tr>
                    <td>性别:</td>
                    <td>
                        <input type="text" id="input_sex" name="gender" value="1" style="display:none;">
                        <select id="select_sex">
                            <option value="1">男</option>
                            <option value="0">女</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>年龄:</td>
                    <td><input type="text" name="age"></td>
                </tr>
                <tr>
                    <td>角色:</td>
                    <td>
                        <input type="text" id="input_role" name="role_id" style="display:none;"/>
                        <select id="id_select_role"></select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input type="submit" style="margin-top: 5px;" value="添加"></td>
                </tr>
            </table>

        </form>

    </div>

</div>
<!--用户-->
<!--<button onclick="deleteAllRowsOfTable('table1')">ooooooooooo</button>-->
<table class="providerTable" id="table1" cellpadding="0" cellspacing="0">
    <tr class="firstTr">
        <th width="10%">用户编号</th>
        <th width="20%">用户名</th>
        <th width="10%">性别</th>
        <th width="10%">年龄</th>
        <th width="10%">电话</th>
        <th width="10%">用户类型</th>
        <th width="10%">操作</th>
    </tr>

</table>
<%--
<nav aria-label="Page navigation">
    <ul class="pagination">
        <li>
            <a href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
            </a>
        </li>
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li>
            <a href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
            </a>
        </li>
    </ul>
</nav>--%>
</div>
</body>
<script type="text/javascript">
    //性别
    $("#select_sex").change(function () {
        var v = $("#select_sex option:selected").val();
        console.log(v);
        $("#input_sex").val(v);
    });
    //用户类型
    $("#id_select_role").change(function () {
        var v = $("#id_select_role option:selected").val();
        console.log(v);
        $("#input_role").val(v);
    });

    // 弹窗
    function showWindow(showmsg) {
        $('#showdiv').show(); //显示弹窗
        $('.content').append(showmsg); //追加内容
        $('#cover').css('display', 'block'); //显示遮罩层
        document.twForm.action = "../addUser";
    }

    // 关闭弹窗
    function closeWindow() {
        $('#showdiv').hide(); //隐藏弹窗
        $('#cover').css('display', 'none'); //显示遮罩层
        $('#showdiv .content').html(""); //清空追加的内容
    }
    closeWindow();
    //调用
    //showWindow('js弹窗 js弹出DIV,并使整个页面背景变暗');
    //调用
    function disable() {
        document.getElementById("accept").disabled = true;
        document.getElementById("accept").style.background = "#A9A9A9"


    }

    function enable() {
        document.getElementById("accept").disabled = false
        document.getElementById("accept").style.background = "blue"
    }

    function deleteAllRowsOfTable(t_id){
        var table=document.getElementById(t_id);
        var len=table.rows.length;
        for (var i = len-1; i>0; i--){
            table.deleteRow(i);
        }
    }




    /*function queryByName(){
        var username = $("#username").val();
        deleteAllRowsOfTable("table1");
        // console.log(username);
        $.getJSON("/smms/queryUser",{"name":username},function (res){
            console.log(res.o_list);

            var table = $("#table1");
            $(res.o_list).each(function (i,item){
                console.log(i,item);
                var sex = "女";
                if(item.gender == 1){
                    sex="男";
                }
                table.append("<tr><td>"+i+"</td><td>"+item.name+"</td><td>"+sex+"</td><td>"+item.age+"</td><td>"+item.telephone+"</td><td>"+item.role_array+"</td><td><button onclick=\"updateUser("+item+")\">修改</button><button>删除</button></td></tr>")
            });
        });
    }*/

    $(document).ready(function (){

        $.getJSON("selectAllRole",function (res){
            //设置到select
            $("#input_role").val(res.o_list[0].id);
            $(res.o_list).each(function (i,item){
                $("#id_select_role").append("<option value='"+item.id+"'>"+item.rolename+"</option>");
            });

        });

        $.getJSON("queryUserList",function (res){
            var table = $("#table1");
            console.log(res);
            $(res.o_list).each(function (i,item){
                item.role_array=item.role_array==null?"":item.role_array;
                var sex = "女";
                if(item.gender == 1){
                    sex="男";
                }
                table.append("<tr><td>"+(i +1)+
                    "</td><td>"+item.name+
                    "</td><td>"+sex+
                    "</td><td>"+item.age+
                    "</td><td>"+item.telephone+
                    "</td><td>"+item.role.rolename+
                    // "</td><td>"+item.role_array+
                    "</td><td>" +
                    "<a class='update' href='toUpdateUser?id="+item.id+"'>修改</a> <a href='deleteUser?id="+item.id+"' onclick='return del()'>删除</a></td></tr>");
            });
        });

        //修改按钮点击事件
        $(".update").click(function (){
            var url = $(this).attr("href");
            console.log(url);
        });

        //查询按钮点击事件
        $("#queryByName").click(function (){
            deleteAllRowsOfTable('table1');
            var username = $("#username").val();
            $.getJSON("queryUser",{"name":username},function (res) {
                var table = $("#table1");


                $(res.o_list).each(function (i,item){
                    item.role_array=item.role_array==null?"":item.role_array;
                    var sex = "女";
                    if(item.gender == 1){
                        sex="男";
                    }
                    table.append("<tr><td>"+(i +1)+
                        "</td><td>"+item.name+
                        "</td><td>"+sex+
                        "</td><td>"+item.age+
                        "</td><td>"+item.telephone+
                        // "</td><td>"+item.role_array+
                        "</td><td>"+item.role.rolename+
                        "</td><td>" +
                        "<a class='update' href='toUpdateUser?id="+item.id+"'>修改</a> <a href='deleteUser?id="+item.id+"'  onclick='return del()'>删除</a></td></tr>");
                });
            });
        });

    });

    function addUser(){
        document.twForm.action = "addUser";
        var ret = false;
        //判断必填的是否填完 code name password gender age telephone address
        if($("input[name='code']").val() != "" &&
            $("input[name='name']").val() != "" &&
            $("input[name='password']").val() != "" &&
            $("input[name='gender']").val() != "" &&
            $("input[name='age']").val() != "" &&
            $("input[name='telephone']").val() != "" &&
            $("input[name='address']").val() != ""
        ){
            ret = true;
        }else {
            alert("请将信息填写完整");
        }
        return ret;
    }

    //修该用户信息
    function updateUser(item){
        showWindow("");
        document.twForm.action = "../addUser";
        var myform=$('#myform'); //得到form对象
        var idInput=$("<input type='text' name='id' disabled=\"true\"/>");
        idInput.attr("value",item.id);
        myform.append(idInput);
        //读取信息到弹出框
        $("input[name='code']").val(item.code);
        $("input[name='name']").val(item.name);
        $("input[name='password']").val(item.password);
        $("input[name='gender']").val(item.gender);
        $("input[name='age']").val(item.age);
        $("input[name='telephone']").val(item.telephone);
        $("input[name='address']").val(item.address);
        $("input[type='submit']").val("修改");
        //修改之后提交

    }

</script>
</html>
