<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    %>
    <base href="<%=basePath%>">
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <script src="http://www.jq22.com/jquery/jquery-1.10.2.js"/>
    <script type="text/javascript" src="<%=basePath%>js/user.js"></script>
    <script type="text/javascript" src="<%=basePath%>js/del.js"></script>
    <link rel="stylesheet" href="<%=basePath%>css/public.css"/>
    <link rel="stylesheet" href="<%=basePath%>css/style.css"/>
    <style type="text/css">
        .providerTable {
            width: 100%;
        }

        .tablebody {
            width: 100%;
        }
        a {
            display:inline-block;
            margin:0px 0px 5px 5px;
            padding:6px 8px;
            font-size:14px;
            outline:none;
            text-align:center;
            width:50px;
            line-height:30px;
            cursor: pointer;
        }

        a {
            color:white;
            background-color:rgba(0,64,156,.8);
        }

        a:hover {
            color:white;
            background-color:rgba(255,0,0,.8);
        }
    </style>
</head>


<body>
<div class="search">
    <%--<span>商品类型：</span>
    <input type="text" id="id_providerText" placeholder="请输入商品类型的名称"/>
    <input type="button" value="查询" id="id_queryProvider"/>
    <a href="toAddProvider">添加商品类型</a>
    按照类型查--%>
    类型级别：<select id="level-select"></select>
    商品类型：<select id="example-select"></select>
    <a href="addGoodsType">添加商品类型</a>
</div>

<div class="providerTable" style="overflow-x: auto; overflow-y: auto;">
    <div class="tablebodyBox">
        <table class="tablebody" id="table1" cellpadding="0" cellspacing="0">
            <tr class="firstTr">
                <th width="10%">序号</th>
                <th width="10%">商品类型ID</th>
                <th width="20%">商品类型</th>
                <th width="10%">备注</th>
                <th width="10%">操作</th>
            </tr>
        </table>
    </div>
</div>
</div>
</body>
<script type="text/javascript">

    function deleteAllRowsOfTable(t_id) {
        var table = document.getElementById(t_id);
        var len = table.rows.length;
        for (var i = len - 1; i > 0; i--) {
            table.deleteRow(i);
        }
    }
    var menu_op = $("#example-select");
    var level_op = $("#level-select");
    var example_array = [];
    var level_array = [];
    $(document).ready(function () {

        //goodsType
        $.getJSON("goodsTypeList", function (res) {

            var select = document.getElementById("example-select");
            var level = $("#level-select");
            for(index in example_array) {
                select.options[select.options.length] = new Option(example_array[index], index);
                level.options[level.options.length] = new Option(example_array[index], index);
            }

            console.log(res);
            var table = $("#table1");
            $(res.o_list).each(function (i, item) {
                //menu_op.add
                //level_op
                if($.inArray(item.level,level_array) == -1){
                    console.log("--"+item.level);
                    level_array.push(item.level);
                }
                example_array.push(item);
                //level_array.push(item.level);
                item.note=item.note==null?"":item.note;
                //menu_op.append("<option value ='"+item.type_name+"'><a href="+"selectGoodsTypeById?id="+item.type_id+">"+item.type_name+"</a></option>");
                table.append("<tr><td>" + (i +1) +
                    "</td><td>" + item.type_id +
                    "</td><td>" + item.type_name +
                    "</td><td>" + item.note +
                    "</td><td><a href='toUpdateGoodsType?id="+item.type_id+"'>修改</a><a href='deleteGoodsType?id="+item.type_id+"'  onclick='return del()'>删除</a>" +
                    "</td></tr>")
            });
            for(var i=0;i<level_array.length;i++){
                //if(l != 0){
                    level_op.append("<option value ='"+level_array[i]+"'>"+level_array[i]+"</option>");
               // }
            }
            for(var i=0;i<example_array.length;i++){
                if(level_op.val()==example_array[i].level){
                    var item = example_array[i];
                    menu_op.append("<option value ='"+item.type_name+"'><a href="+"selectGoodsTypeById?id="+item.type_id+">"+item.type_name+"</a></option>");
                }
            }


        });
        $("#level-select").change(function () {
            var obj=document.getElementById('example-select'); obj.options.length=0;
            for(var i=0;i<example_array.length;i++){
                if(level_op.val()==example_array[i].level){
                    var item = example_array[i];
                    menu_op.append("<option value ='"+item.type_name+"'><a href="+"selectGoodsTypeById?id="+item.type_id+">"+item.type_name+"</a></option>");
                }
            }
        });
        //点击查询按钮
        $("#id_queryProvider").click(function () {
            var providerName = $("#id_providerText").val();
            deleteAllRowsOfTable("table1");
            $.getJSON("selectProviderByName", {"name": providerName}, function (res) {
                console.log(res);
                item.note=item.note==null?"":item.note;
                var table = $("#table1");
                $(res.o_list).each(function (i, item) {
                    table.append("<tr><td>" + (i +1) +
                        "</td><td>" + item.type_id +
                        "</td><td>" + item.type_name +
                        "</td><td>" + item.note +
                        "</td><td><a href='toUpdateProvider?id="+item.type_id+"'>修改</a><a href='deleteGoodsType?id="+item.type_id+"'  onclick='return del()'>删除</a>" +
                        "</td></tr>")
                });
            });
        });
    });


</script>
</html>
