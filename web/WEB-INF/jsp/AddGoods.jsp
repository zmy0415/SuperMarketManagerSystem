<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <title>添加商品</title>
</head>
<body>

<div>
<%--    商品名称，商品类型（下拉选择），商品厂商（下拉），单位，数量，采购价，出售价，库存下限，备注--%>
        <table style="margin: 40px auto;">
            <tr>
                <td>商品名称:</td>
                <td><input type="text" name="goods_name"></td>
            </tr>
            <tr>
                <td>商品类型:</td>
                <td>
                    <input type="text" name="kind" id="input_lx" style="display: none;"  />
                    类型级别：<select id="level-select"></select>
                    商品类型：<select id="example-select"></select>
                </td>
            </tr>
            <tr>
                <td>商品厂商:</td>
                <td>
                    <input type="text" name="provider" id="input_provider" style="display: none;" />
                    <select id="provider-select"></select>
                </td>
            </tr>
            <tr>
                <td>单位:</td>
                <td><input type="text" name="goods_unit"></td>
            </tr>
            <tr>
                <td>数量:</td>
                <td><input type="text" name="goods_totalnum"></td>
            </tr>
            <tr>
                <td>采购价:</td>
                <td><input type="text" name="goods_purchase_price"></td>
            </tr>
            <tr>
                <td>出售价:</td>
                <td><input type="text" name="goods_price"></td>
            </tr>
            <tr>
                <td>库存下限:</td>
                <td><input type="text" name="lower_bounds"></td>
            </tr>
            <tr>
                <td>备注（可为空）:</td>
                <td><input type="text" name="goods_note"></td>
            </tr>
            <tr>
                <td colspan="1" align="center"><input type="button" style="margin-top: 5px;" id="add" value="添加"></td>
                <td><%--<a href="goodsList">返回</a>--%>
                    <button onclick="window.history.back();">返回</button>
                </td>
            </tr>
        </table>
</div>
</body>
<script type="text/javascript">

    //4.15:0:44  Cause: java.sql.SQLException: Field 'role_id' doesn't have a default value 添加用户报错

    $(document).ready(function (){

        $("#add").click(function (){
            if(addCheck()){
                //alert("true");
                //获取provider, kind
                //$(“#select option:selected”);
                var kd = $("#example-select option:selected").val();
                var pv = $("#provider-select option:selected").val();
                console.log(kd,pv);
                var data = {
                    goods_name:$("input[name='goods_name']").val(),
                    fk_provider:pv,
                    fk_goods_kind:kd,
                    goods_price:$("input[name='goods_price']").val(),
                    goods_purchase_price: $("input[name='goods_purchase_price']").val(),
                    note:$("input[name='goods_note']").val(),
                    goods_totalnum:$("input[name='goods_totalnum']").val(),
                    goods_unit:$("input[name='goods_unit']").val(),
                    lower_bounds:$("input[name='lower_bounds']").val()
                };
                console.log(data);

                //添加商品之前，查询，如果该名称商品存在就不能添加
                $.get("ensureGoodsByName?name="+$("input[name='goods_name']").val(),function (res){
                    if(res == "true"){
                        $.post("insertGoods",data,function (res){
                            window.history.back();
                            //alert(res);
                        });
                    }else {
                        alert("该商品已经存在，请不要重复添加");
                    }

                });


            }
        });
    });

    var menu_op = $("#example-select");
    var level_op = $("#level-select");
    var example_array = [];
    var level_array = [];
    $(document).ready(function (){
        //获取到select

        $.getJSON("selectAllProvider",function (res){
            var provider_select = $("#provider-select");
            $(res.o_list).each(function (i, item) {
                provider_select.append("<option value ='" + item.id + "'>"+item.proName+"</option>");
            });
        });

        $.getJSON("goodsTypeList", function (res) {

            var select = document.getElementById("example-select");
            var level = $("#level-select");
            for (index in example_array) {
                select.options[select.options.length] = new Option(example_array[index], index);
                level.options[level.options.length] = new Option(example_array[index], index);
            }

            console.log(res);
            var table = $("#table1");
            $(res.o_list).each(function (i, item) {
                //menu_op.add
                //level_op
                if ($.inArray(item.level, level_array) == -1) {
                    console.log("--" + item.level);
                    level_array.push(item.level);
                }
                example_array.push(item);
            });
            for (var i = 0; i < level_array.length; i++) {
                //if(l != 0){
                level_op.append("<option value ='" + level_array[i] + "'>" + level_array[i] + "</option>");
                // }
            }
            for (var i = 0; i < example_array.length; i++) {
                if (level_op.val() == example_array[i].level) {
                    var item = example_array[i];
                    menu_op.append("<option value ='" + item.type_id + "'><a href=" + "selectGoodsTypeById?id=" + item.type_id + ">" + item.type_name + "</a></option>");
                }
            }

        });
        // $('#example-select option').on('click', function() {
        //     document.getElementById('input_lx').value=
        //         document.getElementById('example-select').options[document.getElementById('example-select').selectedIndex].value;
        //
        // });
        // $('#provider-select option').on('click', function() {
        //     console.log(document.getElementById('provider-select').options[document.getElementById('provider-select').selectedIndex].value);
        //     document.getElementById('input_provider').value=
        //         document.getElementById('provider-select').options[document.getElementById('provider-select').selectedIndex].value;
        //
        // });
        $("#level-select").change(function () {
            var obj=document.getElementById('example-select'); obj.options.length=0;
            for(var i=0;i<example_array.length;i++){
                if(level_op.val()==example_array[i].level){
                    var item = example_array[i];
                    menu_op.append("<option value ='"+item.type_id+"'><a href="+"selectGoodsTypeById?id="+item.type_id+">"+item.type_name+"</a></option>");
                }
            }
        });
    });
    function addCheck(){
        var ret = false;

        // console.log(1,$("input[name='goods_name']").val(),
        //     2,$("input[name='kind']").val(),
        //     3,$("input[name='provider']").val(),
        //     4,$("input[name='goods_unit']").val(),
        //     5,$("input[name='goods_totalnum']").val(),
        //     6,$("input[name='goods_purchase_price']").val(),
        //     7,$("input[name='goods_price']").val(),
        //     8,$("input[name='lower_bounds']").val()
        // );
        //判断必填的是否填完 code name password gender age telephone address
        if($("input[name='goods_name']").val() != "" &&
            //类型
            // $("input[name='kind']").val() != "" &&
            // $("input[name='provider']").val() != "" &&
            $("input[name='goods_unit']").val() != "" &&
            $("input[name='goods_totalnum']").val() != "" &&
            $("input[name='goods_purchase_price']").val() != ""&&
            $("input[name='goods_price']").val() != ""&&
            $("input[name='lower_bounds']").val() != ""
        ){
            ret = true;
        }else {
            alert("请将信息填写完整");
        }
        return ret;
    }
</script>
</html>
