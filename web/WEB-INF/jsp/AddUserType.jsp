<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加用户角色类型</title>
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
</head>
<body>
<div>
    <form action="insertRole" name="twForm" id="myform" onsubmit="return addCheck()">
        <table style="margin: 40px auto;">
            <caption>添加用户类型</caption>
            <tr>
                <td>用户类型名称:</td>
                <td><input type="text" name="rolename"></td>
            </tr>
            <tr>
                <td>备注:</td>
                <td><input type="text" name="note"></td>
            </tr>
            <tr>
                <td>权限:</td>
                <td><input type="text" name="permission"></td>
                <select id="id_permission">

                </select>
            </tr>
            <tr>
                <td colspan="1" align="center"><input type="submit" style="margin-top: 5px;" value="添加"></td>
                <td><a href="userType">返回</a></td>
            </tr>
        </table>
    </form>
</div>
</body>
<script>
    function addCheck(){
        var ret = false;

        //判断必填的是否填完 code name password gender age telephone address
        if($("input[name='rolename']").val() != "" &&
            $("input[name='permission']").val() != "" &&
            $("input[name='note']").val() != ""
        ){
            ret = true;
        }else {
            alert("请将信息填写完整");
        }
        return ret;
    }
    //添加权限
    $(document).ready(function (){
        // $.getJSON("selectAllProvider",function (res){
        //     var provider_select = $("#id_permission");
        //     $(res.o_list).each(function (i, item) {
        //         provider_select.append("<option value ='" + item.id + "'>"+item.proName+"</option>");
        //     });
        // });
        // $("#level-select").change(function () {
        //     var obj=document.getElementById('example-select'); obj.options.length=0;
        //     for(var i=0;i<example_array.length;i++){
        //         if(level_op.val()==example_array[i].level){
        //             var item = example_array[i];
        //             menu_op.append("<option value ='"+item.type_id+"'><a href="+"selectGoodsTypeById?id="+item.type_id+">"+item.type_name+"</a></option>");
        //         }
        //     }
        // });
    });

</script>
</html>
