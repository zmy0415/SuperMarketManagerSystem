<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
        <%
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
                    + path + "/";
        %>
        <base href="<%=basePath%>">
    <meta charset="utf-8">
    <title>超市管理系统</title>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://www.imooc.com/static/lib/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<%--
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <script src="<%=basePath%>js/bootstrap.js"></script>--%><%--<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>
    <style>
        .container{
            width: 100%;
            height: 100%;
        }
        /*#iframeDiv iframe, #iframeDiv div {*/
        /*    width: 100%;*/
        /*    height: 100%;*/
        /*}*/
        .tab-pane iframe{
            width: 100%;
            height: 500px;
        }
    </style>
    <script>
        $('.nav').height($(window).height());
    </script>
</head>
<body>
<div id="head">
    <h1>超市管理系统</h1>
</div>
<a href="home2.jsp">sssss</a>
<div class="container" width="100%">
<%--    <div class="row clearfix">--%>
        <div class="col-md-2">
            <ul class="nav nav-tab nav-stacked vertical-tab" id="lmenu">
                <li class="active"><a href="#home" data-toggle="tab">供应商管理</a>
                <li><a href="#li01" data-toggle="tab">用户管理</a>
                <li><a href="#li03" data-toggle="tab">库存管理</a>

                    <%--<ul id="li-menu" class="nav nav-tab nav-stacked vertical-tab" hidden>
                        <li><a href="#li02-li01" data-toggle="tab">&nbsp;&nbsp;&nbsp;Swing</a><>
                        <li><a href="#li02-li02" data-toggle="tab">&nbsp;&nbsp;&nbsp;jMeter</a><>
                    </ul>--%>
                </li>
                <li><a href="#li04" data-toggle="tab">借阅一览</a>
                <li><a href="#li05" data-toggle="tab">用户管理</a>
            </ul>
        </div>
        <%--  内容 --%>
        <div class="tab-content vertical-tab-content col-md-10" id="iframeDiv">
            <div class="tab-pane active" id="home">
                <iframe src="provider" id="iframe_home" framespacing="0" frameborder="no" border="0"></iframe>
            </div>
            <div class="tab-pane" id="li01">
                <iframe src="userList" id="iframe_li01" framespacing="0" frameborder="no" border="0"></iframe>
            </div>
            <div class="tab-pane" id="li03">
                <iframe src="goodsList" framespacing="0" frameborder="no" border="0"></iframe>
            </div>
            <div class="tab-pane" id="li04">
                <iframe src="success04" framespacing="0" frameborder="no" border="0"></iframe>
            </div>
            <div class="tab-pane" id="li05">
                <iframe src="success05" framespacing="0" frameborder="no" border="0"></iframe>
            </div>
        </div>
<%--    </div>--%>

<%--</div>--%>

</body>
<script type="text/javascript">


</script>
</html>

